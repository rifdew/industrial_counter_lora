/*
	LPG_counter Firmware V0.1 ---
	Created: 3 November 2018

	TO DO:
	- Create sub-routine to communicate with display
	- Create sub-routine to read limit_switch status and increment counnts
	- Create
*/

#include <RHReliableDatagram.h>
#include <SPI.h>
#include <RH_RF95.h>
#include <TimerOne.h>

#define CLOCK_PIN		5
#define DATA_PIN		3
#define LATCH_PIN		2
#define STOP_PIN		0
#define START_PIN		1
#define LIMIT_PIN		12
#define LED_BUILTIN		13

/*
 * LoRa module Pin definition
 */
#define RFM95_CS 		8
#define RFM95_RST 		4
#define RFM95_INT 		7

// Change to 434.0 or other frequency, must match RX's freq!
#define RF95_FREQ 433.0

// Datagram setup
#define	CLIENT_ADDRESS	134
#define SERVER_ADDRESS	1

// Communication Identifier (Start byte)
#define ALIVE			0xFF
#define START_COUNT		0x01
#define P_COUNT			0x02 // Followed by [high] [low] of count
#define STOP_COUNT		0x03
#define ACK				0x04
#define MAX_DATA_LEN	3

// Singleton instance of the radio driver
RH_RF95 driver;

// Class to manage message delivery and receipt, using the driver declared above
RHReliableDatagram manager(driver, CLIENT_ADDRESS);

uint16_t count = 0;
uint16_t _prev_count = count;
uint8_t _is_send_to_server = 0; // status for sending data to server every one second

// 4 cases for data transmission (only one of four this variable can be true)
uint8_t _counter_status = 0; // 0->Alive, 1->Started, 2->Counting, 3->Stopped
uint8_t _is_counter_running = 0;

void radio_config(){
if (!manager.init()){
	Serial.println("init failed");
}

// Defaults after init are 434.0MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on

// The default transmitter power is 13dBm, using PA_BOOST.
// If you are using RFM95/96/97/98 modules which uses the PA_BOOST transmitter pin, then
// you can set transmitter powers from 5 to 23 dBm:
driver.setTxPower(23, false); // set for maximum power

// If you are using Modtronix inAir4 or inAir9,or any other module which uses the
// transmitter RFO pins and not the PA_BOOST pins
// then you can configure the power transmitter power for -1 to 14 dBm and with useRFO true.
// Failure to do that will result in extremely low transmit powers.
// driver.setTxPower(14, true);

// You can optionally require this module to wait until Channel Activity
// Detection shows no activity on the channel before transmitting by setting
// the CAD timeout to non-zero:
//  driver.setCADTimeout(10000);
}

void io_config(){
	pinMode(CLOCK_PIN, OUTPUT);
	pinMode(DATA_PIN, OUTPUT);
	pinMode(LATCH_PIN, OUTPUT);

	pinMode(STOP_PIN, INPUT);
	pinMode(START_PIN, INPUT);
	pinMode(LIMIT_PIN, INPUT);

	attachInterrupt(digitalPinToInterrupt(START_PIN), enable_counter, RISING);
	attachInterrupt(digitalPinToInterrupt(STOP_PIN), reset_counter, FALLING);

	Timer1.initialize(1000000); // send data to server every one second
  	Timer1.attachInterrupt(send_data);
}

void io_test(){
	digitalWrite(CLOCK_PIN, HIGH);
	digitalWrite(DATA_PIN, HIGH);
	digitalWrite(LATCH_PIN, HIGH);
	delay(2000);

	digitalWrite(CLOCK_PIN, LOW);
	digitalWrite(DATA_PIN, LOW);
	digitalWrite(LATCH_PIN, LOW);
	delay(2000);

	bool limit_status 	= digitalRead(LIMIT_PIN);
	bool start_status 	= digitalRead(START_PIN);
	bool stop_status 	= digitalRead(STOP_PIN);

	Serial.print("limit_status: ");
	Serial.print(limit_status);
	Serial.print("; start_status: ");
	Serial.print(start_status);
	Serial.print("; stop_status: ");
	Serial.print(stop_status);
	Serial.print("\n");
}

void counter_config(){
	TCCR1B |= (1<<CS12) | (1<<CS11) | (1<<CS10);
	TCNT1 = 0;
}
// Bit-bang Function
void display_digit(uint16_t _send_this){
	uint16_t _temp = 0;
	// first digit is the most significance number
	byte _byte_send_this[4] = {0, 0, 0, 0};
	_byte_send_this[0] = (byte)(_send_this/1000);
	_temp = _send_this % 1000;
	_byte_send_this[1] = (byte)(_temp/100);
	_temp = _temp % 100;
	_byte_send_this[2] = (byte)(_temp/10);
	_byte_send_this[3] = (byte)(_send_this%10);

	// Test for algorithm to
	// Serial.print(_byte_send_this[0]);
	// Serial.print(_byte_send_this[1]);
	// Serial.print(_byte_send_this[2]);
	// Serial.print(_byte_send_this[3]);
	// Serial.print("\n");

	// Set LATCH_PIN to HIGH
	digitalWrite(LATCH_PIN, HIGH);
	for(int i=0; i<4; i++){
		// 8 bits in a byte
		Serial.print("\n");

	  	for(int j=7; j>=0; j--){
			digitalWrite(DATA_PIN, bitRead(_byte_send_this[i], j)); // Write data bit[n] to data line
			// Serial.print(bitRead(_byte_send_this[i], j));
			delayMicroseconds(300); // delay 300us
			digitalWrite(CLOCK_PIN, HIGH); // set CLOCK_PIN HIGH

			// check if at the end of transmission
			if(i==3 && j==0){
				delayMicroseconds(250); // delay 250 us (ta)
				digitalWrite(LATCH_PIN, LOW); // set LATCH_PIN LOW
				delayMicroseconds(150); // delay 150 us (0.5*tw)
				digitalWrite(CLOCK_PIN, LOW); // set CLOCK_PIN LOW
				delayMicroseconds(150); // delay 150 us (0.5*tw)
				digitalWrite(LATCH_PIN, HIGH); // set LATCH_PIN HIGH
			}
			else{
				delayMicroseconds(300); // delay 600us
				digitalWrite(CLOCK_PIN, LOW);// set CLOCK_PIN LOW
			}
	  	}
	}
}

void jokes_jorok(){
	uint16_t _temp = 0;
	// first digit is the most significance number
	byte _byte_send_this[4] = {0, 0, 0, 0};
	_byte_send_this[0] = 13;
	_byte_send_this[1] = 10;
	_byte_send_this[2] = 13;
	_byte_send_this[3] = 10;

	// Test for algorithm to
	// Serial.print(_byte_send_this[0]);
	// Serial.print(_byte_send_this[1]);
	// Serial.print(_byte_send_this[2]);
	// Serial.print(_byte_send_this[3]);
	// Serial.print("\n");

	// Set LATCH_PIN to HIGH
	digitalWrite(LATCH_PIN, HIGH);
	for(int i=0; i<4; i++){
		// 8 bits in a byte
		Serial.print("\n");

	  	for(int j=7; j>=0; j--){
			digitalWrite(DATA_PIN, bitRead(_byte_send_this[i], j)); // Write data bit[n] to data line
			// Serial.print(bitRead(_byte_send_this[i], j));
			delayMicroseconds(300); // delay 300us
			digitalWrite(CLOCK_PIN, HIGH); // set CLOCK_PIN HIGH

			// check if at the end of transmission
			if(i==3 && j==0){
				delayMicroseconds(250); // delay 250 us (ta)
				digitalWrite(LATCH_PIN, LOW); // set LATCH_PIN LOW
				delayMicroseconds(150); // delay 150 us (0.5*tw)
				digitalWrite(CLOCK_PIN, LOW); // set CLOCK_PIN LOW
				delayMicroseconds(150); // delay 150 us (0.5*tw)
				digitalWrite(LATCH_PIN, HIGH); // set LATCH_PIN HIGH
			}
			else{
				delayMicroseconds(300); // delay 600us
				digitalWrite(CLOCK_PIN, LOW);// set CLOCK_PIN LOW
			}
	  	}
	}
}

void welcome_screen(){
	uint16_t _temp = 0;
	// first digit is the most significance number
	byte _byte_send_this[4] = {0, 0, 0, 0};
	_byte_send_this[0] = 27;
	_byte_send_this[1] = 13;
	_byte_send_this[2] = 63;
	_byte_send_this[3] = 63;

	// Test for algorithm to
	// Serial.print(_byte_send_this[0]);
	// Serial.print(_byte_send_this[1]);
	// Serial.print(_byte_send_this[2]);
	// Serial.print(_byte_send_this[3]);
	// Serial.print("\n");

	// Set LATCH_PIN to HIGH
	digitalWrite(LATCH_PIN, HIGH);
	for(int i=0; i<4; i++){
		// 8 bits in a byte
		Serial.print("\n");

	  	for(int j=7; j>=0; j--){
			digitalWrite(DATA_PIN, bitRead(_byte_send_this[i], j)); // Write data bit[n] to data line
			// Serial.print(bitRead(_byte_send_this[i], j));
			delayMicroseconds(300); // delay 300us
			digitalWrite(CLOCK_PIN, HIGH); // set CLOCK_PIN HIGH

			// check if at the end of transmission
			if(i==3 && j==0){
				delayMicroseconds(250); // delay 250 us (ta)
				digitalWrite(LATCH_PIN, LOW); // set LATCH_PIN LOW
				delayMicroseconds(150); // delay 150 us (0.5*tw)
				digitalWrite(CLOCK_PIN, LOW); // set CLOCK_PIN LOW
				delayMicroseconds(150); // delay 150 us (0.5*tw)
				digitalWrite(LATCH_PIN, HIGH); // set LATCH_PIN HIGH
			}
			else{
				delayMicroseconds(300); // delay 600us
				digitalWrite(CLOCK_PIN, LOW);// set CLOCK_PIN LOW
			}
	  	}
	}
}

void blank_display(){
	uint16_t _temp = 0;
	// first digit is the most significance number
	byte _byte_send_this[4] = {0, 0, 0, 0};
	_byte_send_this[0] = 63;
	_byte_send_this[1] = 63;
	_byte_send_this[2] = 63;
	_byte_send_this[3] = 63;

	// Test for algorithm to
	// Serial.print(_byte_send_this[0]);
	// Serial.print(_byte_send_this[1]);
	// Serial.print(_byte_send_this[2]);
	// Serial.print(_byte_send_this[3]);
	// Serial.print("\n");

	// Set LATCH_PIN to HIGH
	digitalWrite(LATCH_PIN, HIGH);
	for(int i=0; i<4; i++){
		// 8 bits in a byte
		Serial.print("\n");

	  	for(int j=7; j>=0; j--){
			digitalWrite(DATA_PIN, bitRead(_byte_send_this[i], j)); // Write data bit[n] to data line
			// Serial.print(bitRead(_byte_send_this[i], j));
			delayMicroseconds(300); // delay 300us
			digitalWrite(CLOCK_PIN, HIGH); // set CLOCK_PIN HIGH

			// check if at the end of transmission
			if(i==3 && j==0){
				delayMicroseconds(250); // delay 250 us (ta)
				digitalWrite(LATCH_PIN, LOW); // set LATCH_PIN LOW
				delayMicroseconds(150); // delay 150 us (0.5*tw)
				digitalWrite(CLOCK_PIN, LOW); // set CLOCK_PIN LOW
				delayMicroseconds(150); // delay 150 us (0.5*tw)
				digitalWrite(LATCH_PIN, HIGH); // set LATCH_PIN HIGH
			}
			else{
				delayMicroseconds(300); // delay 600us
				digitalWrite(CLOCK_PIN, LOW);// set CLOCK_PIN LOW
			}
	  	}
	}
}

uint8_t send_to_server(){
	uint8_t data_length = 0;
	uint8_t data[MAX_DATA_LEN] = {};
	// determine which state is the counter now and send proper data to server
	switch (_counter_status){
		case 0: // Alive
			data_length = 1;
			data[0] = ALIVE;
			data[1] = 0;
			data[2] = 0;
			_counter_status = 1;
			break;
		case 1: // Started
			data_length = 1;
			data[0] = START_COUNT;
			data[1] = 0;
			data[2] = 0;
			_counter_status = 2;
			break;
		case 2: // Running
			data_length = 3;
			data[0] = P_COUNT;
			data[1] = (uint8_t)((count >> 0x08) & 0xff); // High byte
			data[2] = (uint8_t)(count & 0xff); // Low byte
			break;
		case 3: // Stopped
			data_length = 1;
			data[0] = STOP_COUNT;
			data[1] = 0;
			data[2] = 0;
			_counter_status = 0;
			break;
	}


	// send data to server
	if (manager.sendtoWait(data, data_length, SERVER_ADDRESS)){
		// Wait for reply
		uint8_t len = MAX_DATA_LEN;

		// if ACK then wait for another second to transmit next data_length
		if (manager.recvfromAckTimeout(uint8_t *buf, uint8_t *len, uint16_t timeout))
		// if not then resend data
	}
}

void setup(){
	Serial.begin(9600); // For debugging only
  	io_config();
	radio_config();
	counter_config();
  	pinMode(LED_BUILTIN, OUTPUT);


	// Keinginan manusia untuk vandalisme
	welcome_screen();
	delay(500);
	blank_display();
	delay(250);
	display_digit(134);
	delay(500);
	blank_display();
	delay(250);

	display_digit(count);
}

void loop(){
	// io_test(); // Tested OK!

	// Check if counter is running and add count
	if(TCNT1 != 0 && _is_counter_running == 1){
		count += TCNT1;
		TCNT1 = 0; // reset buffer

		// reset counter if it overflow
		if (count > 9999){
			count = 0;
		}
	}

	if(_prev_count != count){
		display_digit(count);
		_prev_count = count;
	}

	// if one second elapsed from last transmission then send data to server
	if(_is_send_to_server == 1){
		send_to_server();
	}

	// debug here
	// Serial.println(count);
}

// All interrupt service routine function below
void enable_counter(){
	if (_is_counter_running == 0){
		_is_counter_running = 1;
		TCNT1 = 0;
		count = 0;
		_is_counter_stopped = 0;
	}
}

void reset_counter(){
	if(_is_counter_running == 1){
		_is_counter_running = 0;
		TCNT1 = 0;
		count = 0;
		_is_counter_stopped = 1;
	}
}

void send_data(){
	if(_is_send_to_server == 0){
		_is_send_to_server = 1;
	}
}
